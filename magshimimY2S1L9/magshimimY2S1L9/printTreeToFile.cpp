#include "printTreeToFile.h"
#include <fstream>

void printTreeToFile(const BSNode* bs, std::string output)
{
	std::ofstream myFile;
	myFile.open(output);
	print(bs, &myFile);
	myFile.close();
}

void print(const BSNode* bs, std::ofstream* myFile)
{
	if (bs == NULL)
		return;
	*myFile << bs->getData() << " "; // first root
	print(bs->getLeft(), myFile); //then left

	*myFile << "# ";
	print(bs->getRight(), myFile); //then right
}

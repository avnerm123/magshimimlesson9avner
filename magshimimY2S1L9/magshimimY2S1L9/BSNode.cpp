#include "BSNode.h"
#include <iostream>

BSNode::BSNode(std::string data)
{
	this->_count = 1;
	this->_data = data;
	this->_left = this->_right = NULL;
}

BSNode::BSNode(const BSNode& other)
{
	 //using the = operator
	*this = (BSNode&)other;

}

BSNode::BSNode()
{
	this->_data = "0";
	this->_left = this->_right = NULL;
}

BSNode::~BSNode()
{
	if (this == NULL)
	{
		return;
	}
	else
	{
		if (this->getRight() != NULL)
		{
			delete this->getRight();
		}
		else
		{
		}
		if (this->getLeft() != NULL)
		{
			delete this->getLeft();
		}
	}
}

void BSNode::insert(std::string value)
{
	BSNode inserted(value);
	if (this == nullptr) {
		return;
	}
	if (this->search(value)) {
		this->_count++;
		return;
	}
	this->InsertNode(&inserted);
}


void BSNode::InsertNode(BSNode* node)
{
	if (this == nullptr)
		return;
	else if (this->getData() > node->getData())
	{
		if (this->getLeft() == NULL)
		{
			this->_left = new BSNode(node->getData());
		}
		else
		{
			this->getLeft()->InsertNode(node);
		}
	}
	else
	{
		if (this->getRight() == NULL)
		{
			this->_right = new BSNode(node->getData());
		}
		else
		{
			this->getRight()->InsertNode(node);
		}
	}
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (this == nullptr)
	{
		return 0;
	}
	if (this->getData() == node->getData())
	{
		return 1;
	}
	else
	{
		if (this->getLeft() != nullptr && this->getData() > node->getData())
		{
			return 1 + this->getLeft()->getCurrNodeDistFromInputNode(node);
		}
		else if (this->getRight() != nullptr && this->getData() < node->getData())
		{
			return 1 + this->getRight()->getCurrNodeDistFromInputNode(node);
		}
	}
	return 0;
}

int BSNode::getDepth(const BSNode& root) const
{
	if(root.search(this->getData())){return ERROR;}
	return this->getCurrNodeDistFromInputNode(&root);

}

BSNode& BSNode::operator=(BSNode& other)
{
	this->_data = other.getData();

	if (this->_left)
		this->_left = other.getLeft();

	if (this->_right)
		this->_right = other.getRight();

	return *this;
}


bool BSNode::isLeaf() const
{
	return this->getRight() == NULL && this->getLeft() == NULL;
}


std::string BSNode::getData() const
{
	return this->_data;
}


BSNode* BSNode::getLeft() const
{
	return this->_left;
}


BSNode* BSNode::getRight() const
{

	return this->_right;
}


bool BSNode::search(std::string val) const
{
		
	if (this->getData().compare(val) > 0)
		if(this->getLeft() != NULL)
			this->getLeft()->search(val);
	if(this->getRight() != NULL)
		this->getRight()->search(val);
	return this->_data.compare(val) == 0;
}


int BSNode::getHeight() const
{
	if (this == nullptr)
		return 0;
	else
	{
		/* compute the depth of each subtree */
		int lDepth = this->_left->getHeight();
		int rDepth = this->_right->getHeight();

		/* use the larger one */
		if (lDepth > rDepth)
			return(lDepth + 1);
		else return(rDepth + 1);
	}
}





int BSNode::getCount() const
{
	return this->_count;
}

void BSNode::printNodes() const
{
	if (this == nullptr)
		return;
	this->_left->printNodes();
	std::cout << this->getData() << " " << this->getCount() << std::endl;
	this->_right->printNodes();
}



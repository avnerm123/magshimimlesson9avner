#pragma once

#include <string>

#define ERROR -1

class BSNode
{
public:
	BSNode(std::string data);
	BSNode(const BSNode& other);
	BSNode();
	~BSNode();
	
	void insert(std::string value);
	BSNode& operator=(BSNode& other);
	
	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(std::string val) const;

	int getHeight() const;

	int getDepth(const BSNode& root) const;
	int getCount() const;
	void printNodes() const; //for question 1 part C

	void InsertNode(BSNode* node);

private:
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth
	std::string _data;
	BSNode* _left;
	BSNode* _right;

	int _count = 0; //for question 1 part B

};
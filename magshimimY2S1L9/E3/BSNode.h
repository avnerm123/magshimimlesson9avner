#include <string>
#include <iostream>
#define ERROR -1

template<class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode<T>& other);
	BSNode();
	~BSNode();

	void insert(T value);
	BSNode<T>& operator=(BSNode<T>& other);

	bool isLeaf() const;
	T getData() const;
	BSNode<T>* getLeft() const;
	BSNode<T>* getRight() const;

	bool search(T) const;

	int getHeight() const;

	int getDepth(const BSNode<T>& root) const;
	int getCount() const;
	void printNodes() const; //for question 1 part C

	void InsertNode(BSNode<T>* node);

private:
	int getCurrNodeDistFromInputNode(const BSNode<T>* node) const; //auxiliary function for getDepth
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;

	int _count = 0; //for question 1 part B

};


template <class T>
BSNode<T>::BSNode(T data)
{
	this->_count = 1;
	this->_data = data;
	this->_left = this->_right = NULL;
}

template <class T>
BSNode<T>::BSNode(const BSNode& other)
{
	//using the = operator
	*this = (BSNode&)other;

}

template <class T>
BSNode<T>::BSNode()
{
	this->_left = this->_right = NULL;
}

template <class T>
BSNode<T>::~BSNode()
{
	if (this == NULL)
	{
		return;
	}
	else
	{
		if (this->getRight() != NULL)
		{
			delete this->getRight();
		}
		else
		{
		}
		if (this->getLeft() != NULL)
		{
			delete this->getLeft();
		}
	}
}

template <class T>
void BSNode<T>::insert(T value)
{
	BSNode inserted(value);
	if (this == nullptr) {
		return;
	}
	if (this->search(value)) {
		this->_count++;
		return;
	}
	this->InsertNode(&inserted);
}

template <class T>
void BSNode<T>::InsertNode(BSNode* node)
{
	if (this == nullptr)
		return;
	else if (this->getData() > node->getData())
	{
		if (this->getLeft() == NULL)
		{
			this->_left = new BSNode(node->getData());
		}
		else
		{
			this->getLeft()->InsertNode(node);
		}
	}
	else
	{
		if (this->getRight() == NULL)
		{
			this->_right = new BSNode(node->getData());
		}
		else
		{
			this->getRight()->InsertNode(node);
		}
	}
}

template <class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (this == nullptr)
	{
		return 0;
	}
	if (this->getData() == node->getData())
	{
		return 1;
	}
	else
	{
		if (this->getLeft() != nullptr && this->getData() > node->getData())
		{
			return 1 + this->getLeft()->getCurrNodeDistFromInputNode(node);
		}
		else if (this->getRight() != nullptr && this->getData() < node->getData())
		{
			return 1 + this->getRight()->getCurrNodeDistFromInputNode(node);
		}
	}
	return 0;
}

template <class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	if (root.search(this->getData())) { return ERROR; }
	return this->getCurrNodeDistFromInputNode(&root);

}

template <class T>
BSNode<T>& BSNode<T>::operator=(BSNode<T>& other)
{
	this->_data = other.getData();

	if (this->_left)
		this->_left = other.getLeft();

	if (this->_right)
		this->_right = other.getRight();

	return *this;
}

template <class T>
bool BSNode<T>::isLeaf() const
{
	return this->getRight() == NULL && this->getLeft() == NULL;
}

template <class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template <class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template <class T>
BSNode<T>* BSNode<T>::getRight() const
{

	return this->_right;
}

template <class T>
bool BSNode<T>::search(T val) const
{

	if (this->getData() > val)
		if (this->getLeft() != NULL)
			this->getLeft()->search(val);
	if (this->getRight() != NULL)
		this->getRight()->search(val);
	return this->_data == val;
}

template <class T>
int BSNode<T>::getHeight() const
{
	if (this == nullptr)
		return 0;
	else
	{
		/* compute the depth of each subtree */
		int lDepth = this->_left->getHeight();
		int rDepth = this->_right->getHeight();

		/* use the larger one */
		if (lDepth > rDepth)
			return(lDepth + 1);
		else return(rDepth + 1);
	}
}




template <class T>
int BSNode<T>::getCount() const
{
	return this->_count;
}

template <class T>
void BSNode<T>::printNodes() const
{
	if (this == nullptr)
		return;
	this->_left->printNodes();
	std::cout << this->getData() << " " << this->getCount() << " \\";
	this->_right->printNodes();
}



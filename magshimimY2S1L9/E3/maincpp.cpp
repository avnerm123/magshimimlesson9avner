#include <iostream>
#include "BSNode.h"


#define SIZE 15

int main()
{
	int arr[SIZE] = { 2, 5, 6, 7, 8, 9, 10, -1, 1, -11, -34, -12, 23,14, 15};
	std::string carr[SIZE] = { "2", "5", "6", "7", "8", "9", "10", "1", "1", "11", "34", "12", "23", "14", "15" };

	for (int i = 0; i < SIZE; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;

	for (int i = 0; i < SIZE; i++)
	{
		std::cout << carr[i] << " ";
	}
	std::cout << std::endl;

	BSNode<int>intBT;
	BSNode<std::string>stBT;

	for (int i = 0; i < SIZE; i++)
	{
		intBT.insert(arr[i]);
		stBT.insert(carr[i]);
	}

	intBT.printNodes();
	std::cout << std::endl;
	stBT.printNodes();

	return 0;
}
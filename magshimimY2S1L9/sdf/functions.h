#pragma once
#include <iostream>


template<typename T>
int compare(T first, T second)
{
	if (first == second)
	{
		return 0;
	}
	if (first > second)
	{
		return -1;
	}
	if (first < second)
	{
		return 1;
	}

}

template<typename T>
void bubbleSort(T arr[], int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				T temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

template<typename T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}
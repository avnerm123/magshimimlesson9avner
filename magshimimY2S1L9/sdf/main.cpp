
#include <iostream>

#include "functions.h"


class myClass{
public:
	int member;
	myClass(int member)
	{
		this->member = member;
	}

	bool operator==(myClass a)
	{
		return a.member == this->member;
	}

	bool operator>(myClass a)
	{
		return (this->member > a.member);
	}

	bool operator<(myClass a)
	{
		return (this->member < a.member);
	}

	myClass& operator=(myClass a)
	{
		this->member = a.member;
		return *this;
	}
	
	friend std::ostream& operator<<(std::ostream& out, myClass a)
	{
		return out << a.member << " ";
	}

};

int main() {
	//floats checking
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	//char checking:
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('b', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	char charArr[arr_size] = { 'e', 'b', 'c', 'd', 'a' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	//checking  myclass

	myClass first(5);
	myClass second(10);
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<myClass>(first, second) << std::endl;
	std::cout << compare<myClass>(second, first) << std::endl;
	std::cout << compare<myClass>(first, second) << std::endl;

	myClass myArr[arr_size] = { myClass(10), myClass(15), myClass(3), myClass(5), myClass(-7) };

	bubbleSort<myClass>(myArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << myArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<myClass>(myArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}